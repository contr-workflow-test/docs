---
title: Extension points for the admin console
platform: cloud
product: jiracloud
category: reference
subcategory: modules
aliases:
- /jiracloud/jira-platform-modules-administration-console-39988333.html
- /jiracloud/jira-platform-modules-administration-console-39988333.md
confluence_id: 39988333
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39988333
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39988333 
date: "2016-09-15"
---
# Extension points for the admin console

This pages lists the extension points for modules on the JIRA administration console.


## Add-ons

Adds sections and items to the 'Add-ons' menu of the JIRA administration area.

#### Module type
`webSection` + `webPanel`

#### Screenshot
<img src="../images/jira-admin-addons.png"/> 

#### Sample JSON
``` json
...
"modules": {
    "webSections": [
        {
            "key": "example-menu-section",
            "location": "admin_plugins_menu",
            "name": {
                "value": "Example add-on name"
            }
        }
    ],
    "webItems": [
        {
            "key": "example-section-link",
            "location": "admin_plugins_menu/example-menu-section",
            "name": {
                "value": "Example add-on link"
            },
            "url": "/example-section-link"
        }
    ]
}
...
```

#### Properties

`key`

-   **Type**: `string (^[a-zA-Z0-9-]+$)`
-   **Required**: yes
-   **Description**: A key to identify this module. This key must be unique relative to the add on, with the exception of Confluence macros: their keys need to be globally unique. Keys must only contain alphanumeric characters and dashes.

`location`

-   **Description**: For the webSection, set the location to `admin_plugins_menu`. For each webPanel, set the location to the key of the webSection.

`name`

-   **Type**: [i18n property]
-   **Required**: yes
-   **Description**: A human readable name. 

`url`

-   **Type**: `string`, `uri-template`
-   **Required**: yes
-   **Description**: Target URL for the menu item.


  [i18n property]: /cloud/jira/platform/connect/modules/i18n-property
  [additional context]: /cloud/jira/platform/context-parameters
