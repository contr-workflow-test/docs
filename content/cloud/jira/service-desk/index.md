---
title: Latest updates for JIRA Service Desk
platform: cloud
product: jsdcloud
category: devguide
subcategory: index
date: "2017-01-18"
---

# Latest updates

We deploy updates to JIRA Service Desk Cloud frequently. As a JIRA developer, it's important that you're aware of the changes. The resources below will help you keep track of what's happening.

## Recent changes

#### Non-experimental APIs

We have removed the experimental flag from the following APIs:

* `/servicedeskapi/organization`
* `/servicedeskapi/servicedesk/{serviceDeskId}/organization`
* `/servicedeskapi/servicedesk/{serviceDeskId}/queue`
* `/servicedeskapi/servicedesk/{serviceDeskId}/requesttypegroup`

#### New REST APIs

We have added the following APIs:

* `/servicedeskapi/customer`
* `/servicedeskapi/request/{issueIdOrKey}/approval`
* `/servicedeskapi/request/{issueIdOrKey}/transition`
* `/servicedeskapi/servicedesk/{serviceDeskId}/customer`

For up-to-date and complete docs, see the [JSD REST API documentation](https://docs.atlassian.com/jira-servicedesk/REST/cloud/).

#### Platform
Also see the [latest updates for the JIRA Platform](../platform).

## Atlassian Developer blog

Major changes that affect JIRA Cloud developers are announced in the **Atlassian Developer blog**, like new JIRA modules or the deprecation of API end points.
You'll also find handy tips and articles related to JIRA development.

Check it out and subscribe here: [Atlassian Developer blog](https://developer.atlassian.com/blog/)
 ([JIRA-related posts](https://developer.atlassian.com/blog/categories/jira/))

## What's new blog

Major changes that affect all users of the JIRA Cloud products are announced in the *What's New blog* for Atlassian Cloud. This includes new features, bug fixes, and other changes. For example, the introduction of a new JIRA quick search or a change in project navigation. 

Check it out and subscribe here: [What's new blog](https://confluence.atlassian.com/display/Cloud/What%27s+New) (Note, this blog also includes changes to other Cloud applications)


