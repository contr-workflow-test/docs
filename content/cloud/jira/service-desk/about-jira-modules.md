---
title: About JIRA modules
platform: cloud
product: jsdcloud
category: reference
subcategory: modules
date: "2016-10-31"
---

{{< reuse-page path="docs/content/cloud/jira/platform/about-jira-modules.md">}}

## JIRA Service Desk modules

-   [Agent view]
-   [Customer portal]
-   [Automation action]

  [Agent view]: /cloud/jira/service-desk/agent-view
  [Customer portal]: /cloud/jira/service-desk/customer-portal
  [Automation action]: /cloud/jira/service-desk/automation-action