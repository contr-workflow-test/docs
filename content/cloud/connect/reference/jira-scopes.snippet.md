# Scopes

Scopes allow an add-on to request a particular level of access to an Atlassian product.

* Within a particular product instance, an administrator may further limit add-on actions, allowing administrators to safely install add-ons they otherwise would not.
* The scopes may allow the *potential* to access beta or non-public APIs that are later changed in or removed from the
Atlassian product. The inclusion of the API endpoint in a scope does not imply that the product makes this endpoint
public. Read the [JIRA API documentation](../jira-cloud-platform-rest-api/) for details.

The following scopes are available for use by Atlassian Connect JIRA add-ons:

* `NONE` &ndash; can access add-on defined data - this scope does not need to be declared in the descriptor.
* `READ` &ndash; can view, browse, read information from JIRA
* `WRITE` &ndash; can create or edit content in JIRA, but not delete them
* `DELETE` &ndash; can delete entities from JIRA
* `PROJECT_ADMIN` &ndash; can administer a project in JIRA
* `ADMIN` &ndash; can administer the entire JIRA instance
* `ACT_AS_USER` &ndash; can enact services on a user's behalf.



### Example

Scopes are declared as a top level attribute of [`atlassian-connect.json` add-on descriptor](../add-on-descriptor/) as in this example:
``` json
    {
        "baseUrl": "http://my-addon.com",
        "key": "atlassian-connect-addon"
        "modules": {},
        "scopes": [
            "read", "write"
        ]
    }
```
### Reference

See [JIRA scopes reference](../jira-rest-api-scopes/) for specific resources and related scopes.